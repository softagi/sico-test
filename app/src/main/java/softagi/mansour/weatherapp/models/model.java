package softagi.mansour.weatherapp.models;

import java.util.List;

public class model
{
    private boolean status;
    private List<countryModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<countryModel> getData() {
        return data;
    }

    public void setData(List<countryModel> data) {
        this.data = data;
    }
}