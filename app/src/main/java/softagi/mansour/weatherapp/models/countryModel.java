package softagi.mansour.weatherapp.models;

import com.google.gson.annotations.SerializedName;

public class countryModel
{
    @SerializedName("id")
    private int countryId;
    @SerializedName("country_name")
    private String countryName;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}