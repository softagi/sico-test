package softagi.mansour.weatherapp.network.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import softagi.mansour.weatherapp.models.model;

public interface retrofitHelper
{
    @GET("api/get/city/country")
    Call<model> getCountries();
}