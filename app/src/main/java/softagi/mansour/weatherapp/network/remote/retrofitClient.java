package softagi.mansour.weatherapp.network.remote;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import softagi.mansour.weatherapp.models.model;

public class retrofitClient
{
    private static retrofitClient retrofitClient;
    private static retrofitHelper helper;

    private retrofitClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://sci.softagi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        helper = retrofit.create(retrofitHelper.class);
    }

    public static retrofitClient getInstance()
    {
        if (retrofitClient == null)
        {
            retrofitClient = new retrofitClient();
        }

        return retrofitClient;
    }

    public static Call<model> getCountries()
    {
        return helper.getCountries();
    }
}