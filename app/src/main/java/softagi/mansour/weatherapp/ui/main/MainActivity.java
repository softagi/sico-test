package softagi.mansour.weatherapp.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import softagi.mansour.weatherapp.R;
import softagi.mansour.weatherapp.models.countryModel;
import softagi.mansour.weatherapp.models.model;
import softagi.mansour.weatherapp.network.remote.retrofitClient;
import softagi.mansour.weatherapp.network.remote.retrofitHelper;

public class MainActivity extends AppCompatActivity
{
    private Call<model> modelCall;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        modelCall = retrofitClient.getInstance().getCountries();
        modelCall.enqueue(new Callback<model>()
        {
            @Override
            public void onResponse(Call<model> call, Response<model> response)
            {
                if (response.isSuccessful() && response.code() == 200)
                {
                    Log.d("data", String.valueOf(response.body().getData().get(1).getCountryName() + " " + response.message() + " " + response.code()));
                    List<countryModel> models = response.body().getData();
                    Toast.makeText(MainActivity.this, "" + response.code(), Toast.LENGTH_SHORT).show();
                } else
                    {
                        Log.d("code", String.valueOf(response.code() + response.message()));
                    }
            }

            @Override
            public void onFailure(Call<model> call, Throwable t)
            {

            }
        });
    }
}